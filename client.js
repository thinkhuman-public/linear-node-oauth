const {Client} = require('pg');

const db = {
    init() {
        const self = this;
        const user = process.env.DB_USER;
        const password = process.env.DB_PASSWORD;
        const host = process.env.DB_HOST;
        const database = process.env.DB_DATABASE;

        const connection_string = `postgresql://${user}:${password}@${host}:5432/${database}`

        self.client = new Client({
            connectionString: connection_string,
        });
        self.connect();
    },
    connect() {
        const self = this;
        self.client.connect().catch((err) => console.error('Error connecting to database:', err));
    },
    
    async insert_token(server_id, state_hash) {
        const self = this;
        await self.client.query('INSERT INTO tokens (server_id, state_hash) VALUES ($1, $2)', [server_id, state_hash]);
    },

    async get_token(server_id) {
        const self = this;
        console.log(server_id);
        const result = await self.client.query('SELECT * FROM tokens WHERE server_id = $1', [server_id]);
        console.log(result.rows)
        return result.rows[0]
    },

    async update_token(server_id, access_token) {
        const self = this;
        await self.client.query('UPDATE tokens SET access_token = $1 WHERE server_id = $2',[
            access_token,
            server_id
        ])
    }

};

module.exports = db;

