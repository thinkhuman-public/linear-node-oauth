# Linear OAuth2 Client

This is a simple OAuth2 client app for the Linear application that allows users to authenticate with Linear and obtain an access token, which can be used to make authorized API requests on behalf of the user. The app also handles saving the access and refresh tokens to a PostgreSQL database.

## Getting Started

To use this app, you will need to have the following:

1. A Linear API client ID and secret
2. A PostgreSQL database to store access tokens and refresh tokens
3. Node.js and npm installed on your machine

## Installation

1. Clone this repository to your local machine.
2. Install the required dependencies by running npm install.
3. Set the required environment variables in a .env file (see below).
4. Run the app with npm start.

## Environment Variables

To configure the app, you will need to set the following environment variables in a .env file:

    LINEAR_API_CLIENT_ID: The client ID for your Linear API OAuth2 client.
    LINEAR_API_CLIENT_SECRET: The client secret for your Linear API OAuth2 client.
    LINEAR_API_REDIRECT_URI: The redirect URI that Linear will send the access token to.
    DB_USER: The username for your PostgreSQL database.
    DB_PASSWORD: The password for your PostgreSQL database.
    DB_HOST: The hostname for your PostgreSQL database.
    DB_DATABASE: The name of the PostgreSQL database to use.

## Usage

To use the app, open your web browser and navigate to http://localhost:${PORT}. From there, you can enter your Linear API client ID and secret, and the app will handle the authentication process for you. The app will also save the access and refresh tokens to the specified PostgreSQL database for future use.

## Contributing

Contributions are welcome! To contribute to this app, please follow these steps:

Fork this repository to your own Gitlab/Github account and clone it to your local machine.
Create a new branch for your changes.
Make your changes and commit them with clear commit messages.
Push your changes to your forked repository.
Submit a pull request to this repository with a description of your changes.

## Credits

This app was created by Thinkhuman LLC.

https://thinkhuman.co