const express = require('express');
const axios = require('axios');
const dotenv = require('dotenv');
const qs = require('qs');
const simple_oauth = require('simple-oauth2')
const db = require('./client.js')
const crypto = require('crypto');

dotenv.config();
db.init();

const LINEAR_CLIENT_ID = process.env.LINEAR_APP_CLIENT_ID
const LINEAR_CLIENT_SECRET = process.env.LINEAR_APP_CLIENT_SECRET
const LINEAR_REDIRECT_URI = process.env.REDIRECT_URI;

const TOKEN_HOST = 'https://linear.app';

const app = express();
const oauth2 = simple_oauth.create({
    client: {
        id: LINEAR_CLIENT_ID,
        secret: LINEAR_CLIENT_SECRET,
    },
    auth: {
        tokenHost: TOKEN_HOST,
        tokenPath: '/oauth/token',
        authorizePath: '/oauth/authorize',
    },
});

// Redirect the user to Linear.app's authorization endpoint
app.get('/authorize', async (req, res) => {

    // We need a server_id from the bot
    const server_id = req.query.server_id
    const entropy = crypto.randomBytes(32).toString('hex');
    
    if(!server_id) {
        res.send('nah');
        return;
    }

    let meta = {
        server_id : server_id,
        entropy : entropy
    }
    let encoded_meta =  JSON.stringify(meta)
    let meta_hash = crypto.createHash('md5').update(encoded_meta).digest("hex");
    
    let state = {
        meta : meta,
        meta_integrity_hash: meta_hash 
    }
    
    // Write the meta_hash to tokens table so we can verify on callback
    await db.insert_token(server_id, meta_hash)
    
    // Convert the state to base64 so we can send via GET
    const base64_state = Buffer.from(JSON.stringify(state)).toString('base64');
    
    const authorization_uri = oauth2.authorizationCode.authorizeURL({
        redirect_uri: LINEAR_REDIRECT_URI,
        client_id: LINEAR_CLIENT_ID,
        state: base64_state,
        scope: 'issues:create',
    });
    res.redirect(authorization_uri);
});

app.get('', async (req, res) => {
    res.send('Hello World. you should not be here')
})

// Handle the callback after the user authorizes the application
app.get('/callback', async (req, res) => {
    let code = req.query.code;
    let encoded_state = req.query.state;
    try {

        let state = JSON.parse(Buffer.from(encoded_state, 'base64').toString('utf8'));
        let integrity_hash = state.meta_integrity_hash
        let meta = state.meta;
        let encoded_meta =  JSON.stringify(meta)
        let meta_hash = crypto.createHash('md5').update(encoded_meta).digest("hex");

        let existing_token = await db.get_token(meta.server_id)

        if(meta_hash === integrity_hash && existing_token.state_hash === integrity_hash) {
            // Set the request body to include the required parameters
            const requestBody = qs.stringify({
                grant_type: 'authorization_code',
                code: code,
                redirect_uri: process.env.REDIRECT_URI,
                client_id: process.env.LINEAR_APP_CLIENT_ID,
                client_secret: process.env.LINEAR_APP_CLIENT_SECRET,
            });

            const tokenEndpoint = 'https://api.linear.app/oauth/token';

            axios.post(tokenEndpoint, requestBody)
            .then((response) => {
                // Process the response from the OAuth2 server
                console.log(response.data);
                const access_token = response.data.access_token;
                const expires_in = response.data.expires_in;

                db.update_token(meta.server_id, access_token)

                res.send("Job Done");
            })
            .catch((error) => {
                res.send(error);
            });
        } else {
            res.send('nononononon');
        }



    } catch (error) {

        console.error(error);
        res.send('Error occurred');
    }
});

// Start the server
app.listen(process.env.PORT, () => {
    console.log(`Server started on port ${process.env.PORT}`);
});

